package Dist::Zilla::Plugin::ProgCriticTests;
use strict;
use warnings;

# ABSTRACT: Gradually enforce coding standards with Dist::Zilla
# VERSION

use 5.008;

use Moose;
extends 'Dist::Zilla::Plugin::InlineFiles';
with 'Dist::Zilla::Role::TextTemplate';

has step_size    => ( is => 'ro', isa => 'Int', default => 0                     );
has severity     => ( is => 'ro', isa => 'Int', default => 5                     );
has exclude      => ( is => 'ro', isa => 'Str'                                   );
has profile      => ( is => 'ro', isa => 'Str'                                   );
has history_file => ( is => 'ro', isa => 'Str', default => '.perlcritic_history' );


around add_file => sub {
    my ($orig, $self, $file) = @_;

    my $test_content = $self->fill_in_string(
        $file->content,
        {
            step_size       => \$self->step_size,
            severity        => \$self->severity,
            exclude         => \$self->exclude,
            profile         => \$self->profile,
            history_file    => \$self->history_file,
        },
    );

    my $mem_file = Dist::Zilla::File::InMemory->new({
        name    => $file->name,
        content => $test_content,
    });

    return $self->$orig($mem_file);
};

__PACKAGE__->meta->make_immutable;
no Moose;
1;

=head1 SYNOPSIS

In C<dist.ini>:

    [ProgCriticTests]
    severity = 1                        # optional : default = 5
    step_size = 1                       # optional : default = 0
    exclude = RequireExplicitPackage    # optional : default = undef
    profile = .critic_profile           # optional : default = undef
    history_file = .perlcritic_history  # optional : default = .perlcritic_history

=head1 DESCRIPTION

Please see Test::Perl::Critic::Progressive on what exactly it does. For you it's
only important to know that by using this plugin you can avoid the creep of bad
coding practices into your distribution and slowly remove those that have made
their way in already, without being forced to fix everything at once.

The plugin automatically creates the needed test file and primes it with all
data it needs to know about your dist as well as the options you give.

=head1 OPTIONS

=head2 severity

A numerical indicator of severity (see Perl::Critic). This is optional. The
default is 5.

=head2 step_size

A numerical indicator of the expected violation reduction step size. (see
T::P::C::P).  This is optional. The default is 0.

=head2 exclude

A string containing a list of space-separated patterns, which are forwarded as
the exclude option to Perl::Critic. This is optional. Default is undefined.

=head2 profile

A string indicating the path of a perlcriticrc file (see Perl::Critic). If the
path seems to be relative (Class::Path) it is prepended by the distribution root
directory, otherwise it is used as is. This is optional. Default is undefined.

=head2 history_file

A string indicating the path of a perlcritic history file (see T::P::C::P). If
the path seems to be relative (Class::Path) it is prepended by the distribution
root directory, otherwise it is used as is. This is optional. Default is
'.perlcritic_history'.

=head1 SUPPORT

I'm usually on irc.perl.org in #distzilla. If you don't see my name (Mithaldu)
I'm still there, just not at the computer. However if you mention my name, as
well as your problem, I'll get back to you as soon as i get back. Alternatively,
sending me an email or a message on GitHub works as well.

The repository for this plugin is located here:

L<http://github.com/wchristian/Dist-Zilla-Plugin-ProgCriticTests>

=cut

__DATA__
___[ xt/author/critic_progressive.t ]___
use strict;
use warnings;
use Test::More;

eval "use Test::Perl::Critic::Progressive ':all'; use Path::Class 'file'; 1;"
  or die $@;

my $exclude = [qw<{{ $exclude }}>];
my $hf      = q<{{ $history_file }}>;
my $pf      = q<{{ $profile }}>;

my %args = ( '-severity' => {{ $severity }} );
$args{-exclude} = $exclude if $exclude;

if ( $hf ) {
    my $hist_file = file( $hf )->is_relative ? file("../$hf") : file($hf);
    set_history_file( "$hist_file" );
}

if ( $pf ) {
    my $profile   = file( $pf )->is_relative ? file("../$pf") : file($pf);
    $args{-profile} = "$profile" if $profile;
}

set_total_step_size( {{ $step_size }} );
set_critic_args( %args ) if keys %args;

progressive_critic_ok();

done_testing;
