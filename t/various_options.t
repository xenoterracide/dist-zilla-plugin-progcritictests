use strict;
use warnings;
use Test::More;
use Test::DZil;
use Test::Script 1.05;
 
my $tzil
    = Builder->from_config(
        {
            dist_root    => 'corpus/a',
        },
        {
            add_files => {
                'source/dist.ini' => simple_ini(['ProgCriticTests' => {
					severity  => 3,
					step_size => 2,
					history_file => '.critic_history',
				}])
            }
        },
    );
 
$tzil->build;
 
is_filelist(
	$tzil->files,
	[ 'xt/author/critic_progressive.t' ],
	'all files present'
);

my $fn
    = $tzil
    ->tempdir
    ->subdir('build')
    ->subdir('xt')
    ->subdir('author')
    ->file('critic_progressive.t')
    ;
 
my $content = $fn->slurp;

like $content, qr/\.critic_history/, 'hist_path';
like $content,
	qr/^[\s[:graph:]]+severity[\s[:graph:]]+3[\s[:graph:]]+$/xms,
	'severity';

script_compiles( '' . $fn->relative, 'check test compiles' );
script_runs    ( '' . $fn->relative, 'check test runs'     );

done_testing;
